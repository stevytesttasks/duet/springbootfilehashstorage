package com.duet.spring.testtask.springbootfilehashstorage.domain;

import lombok.*;

import javax.persistence.*;

@Table
@Entity
@Data
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FileHash {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    long id;
    @ManyToOne
    Client owner;
    String fileName;
    String sha256;
    String md5;
}

package com.duet.spring.testtask.springbootfilehashstorage.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "storage", ignoreUnknownFields = false)
public class StorageProperties {
    private String location = "upload-dir";
    private String token;
}












package com.duet.spring.testtask.springbootfilehashstorage.model.responses;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteFileResponse {
    private HttpStatus status;
    private String message;
}

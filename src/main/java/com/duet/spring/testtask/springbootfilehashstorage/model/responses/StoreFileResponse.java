package com.duet.spring.testtask.springbootfilehashstorage.model.responses;

import lombok.*;

@Data
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StoreFileResponse {
    private String SHA256;
    private String MD5;
    private String xAuthUser;
    private String fileName;
}

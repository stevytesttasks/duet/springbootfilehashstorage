package com.duet.spring.testtask.springbootfilehashstorage.repository;

import com.duet.spring.testtask.springbootfilehashstorage.domain.FileHash;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("fileHashRepository")
public interface FileHashRepository extends JpaRepository<FileHash, Long> {
    @Query(nativeQuery = true,
            value = "SELECT file_name\n" +
                    "FROM file_hash\n" +
                    "WHERE :hash = sha256\n" +
                    "    or :hash = md5")
    List<String> getFileNamesByHash(String hash);
    @Query(nativeQuery = false,
            value = "SELECT filehash " +
                    "FROM FileHash filehash " +
                    "WHERE :hash = filehash.md5 " +
                        "or :hash = filehash.sha256 ")
    List<String> getFileNamesByHash2(String hash);
    @Query(nativeQuery = true,
            value = "SELECT fh.file_name\n" +
                    "FROM file_hash fh, client cl\n" +
                    "WHERE cl.id = fh.owner_id\n" +
                        "AND (:hash = fh.sha256 OR :hash = fh.md5)\n" +
                        "AND :xAuthUser = cl.x_auth_user")
    String findFilePathByHashAndUserId(String hash, String xAuthUser);
}

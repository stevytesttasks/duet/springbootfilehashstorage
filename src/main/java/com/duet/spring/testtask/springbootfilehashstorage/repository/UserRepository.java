package com.duet.spring.testtask.springbootfilehashstorage.repository;

import com.duet.spring.testtask.springbootfilehashstorage.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<Client, Long> {
    Client getFirstByxAuthUser(String xAuthUser);
}

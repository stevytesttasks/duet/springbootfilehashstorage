package com.duet.spring.testtask.springbootfilehashstorage.service;

import com.duet.spring.testtask.springbootfilehashstorage.configuration.StorageProperties;
import com.duet.spring.testtask.springbootfilehashstorage.domain.FileHash;
import com.duet.spring.testtask.springbootfilehashstorage.domain.Client;
import com.duet.spring.testtask.springbootfilehashstorage.exception.StorageException;
import com.duet.spring.testtask.springbootfilehashstorage.model.responses.StoreFileResponse;
import com.duet.spring.testtask.springbootfilehashstorage.repository.FileHashRepository;
import com.duet.spring.testtask.springbootfilehashstorage.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
@Slf4j
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final FileHashRepository fileHashRepository;
    private final UserRepository userRepository;
    private final StorageProperties storageProperties;

    @Autowired
    public FileSystemStorageService(StorageProperties storageProperties,
                                    @Qualifier("fileHashRepository") FileHashRepository fileHashRepository,
                                    @Qualifier("userRepository") UserRepository userRepository) {
        this.rootLocation = Paths.get(storageProperties.getLocation());
        this.fileHashRepository = fileHashRepository;
        this.userRepository = userRepository;
        this.storageProperties = storageProperties;
    }

    @Override
    public boolean deleteFile(String hash, String xAuthUser) {
        Path filePath = findFilePathByHashAndUserId(hash, xAuthUser);
        try {
            Files.delete(filePath);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Path findFilePathByHashAndUserId(String hash, String xAuthUser) {
        String result = "";
        result = storageProperties.getLocation() + File.separator +
                xAuthUser + File.separator +
                fileHashRepository.findFilePathByHashAndUserId(hash, xAuthUser);
        return Paths.get(result);
    }

    @Override
    public StoreFileResponse storeFile(MultipartFile file, String xAuthUser) {
        Client owner = userRepository.getFirstByxAuthUser(xAuthUser);

        if (owner == null) {
            owner = Client.builder().xAuthUser(xAuthUser).build();
            userRepository.save(owner);
            makeDirectory(new File(storageProperties.getLocation(

            )), owner.getXAuthUser());
        }

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(owner.getXAuthUser() + File.separator + filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
        FileHash fileHash = FileHash.builder()
                .fileName(file.getOriginalFilename())
                .owner(owner)
                .md5(calculateHash(file.getOriginalFilename(), "MD5"))
                .sha256(calculateHash(file.getOriginalFilename(), "SHA-256"))
                .build();
        fileHashRepository.save(fileHash);
        return StoreFileResponse.builder()
                .fileName(filename)
                .xAuthUser(xAuthUser)
                .MD5(fileHash.getMd5())
                .SHA256(fileHash.getSha256())
                .build();
    }

    private String calculateHash(String name, String digestType) {
        String result = "";
        String inputString = storageProperties.getToken() + name;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(digestType);
            messageDigest.update(inputString.getBytes());
            byte[] digest = messageDigest.digest();
            result = DatatypeConverter.printHexBinary(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<String> getListFileNamesByHash(String hash) {
        return fileHashRepository.getFileNamesByHash(hash);
    }

    private void makeDirectory(File authDir, String path) {
        File dir = new File(authDir, path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }
}

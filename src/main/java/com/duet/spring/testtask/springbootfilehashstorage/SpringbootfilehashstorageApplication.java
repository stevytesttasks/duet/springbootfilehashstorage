package com.duet.spring.testtask.springbootfilehashstorage;

import com.duet.spring.testtask.springbootfilehashstorage.configuration.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
@EnableJpaRepositories("com.duet.spring.testtask.springbootfilehashstorage.repository")
public class SpringbootfilehashstorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootfilehashstorageApplication.class, args);
    }

}

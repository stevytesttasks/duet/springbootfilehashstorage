package com.duet.spring.testtask.springbootfilehashstorage.controller;

import com.duet.spring.testtask.springbootfilehashstorage.model.responses.DeleteFileResponse;
import com.duet.spring.testtask.springbootfilehashstorage.model.responses.StoreFileResponse;
import com.duet.spring.testtask.springbootfilehashstorage.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/file_hashes")
@Slf4j
public class FileStorageController {

    private final StorageService storageService;

    @Autowired
    public FileStorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/{hash}")
    @ResponseBody
    public ResponseEntity<List<String>> getFile(
            @PathVariable String hash
    ) {
        List<String> listFileNamesByHash = storageService.getListFileNamesByHash(hash);
        if (listFileNamesByHash.size() == 0) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(listFileNamesByHash);
    }

    @DeleteMapping("/{hash}")
    @ResponseBody
    public ResponseEntity<DeleteFileResponse> deleteFile(
            @PathVariable String hash,
            @RequestHeader("X-Auth-User") String userAuthId
    ) {
        if (userAuthId.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        boolean flag = storageService.deleteFile(hash, userAuthId);
        if (!flag) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(
                DeleteFileResponse.builder()
                        .status(HttpStatus.ACCEPTED)
                        .message("File with hash deleted")
                        .build());
    }

    @PostMapping("/")
    @ResponseBody
    public ResponseEntity<StoreFileResponse> postFile(
            @RequestParam("file") MultipartFile file
            , @RequestHeader("X-Auth-User") String userAuthId) {
        if (userAuthId.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        StoreFileResponse response = storageService.storeFile(file, userAuthId);
        return ResponseEntity.ok(response);
    }
}
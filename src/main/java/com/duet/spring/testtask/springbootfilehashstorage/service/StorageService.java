package com.duet.spring.testtask.springbootfilehashstorage.service;

import com.duet.spring.testtask.springbootfilehashstorage.model.responses.StoreFileResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface  StorageService {
    boolean deleteFile(String hash, String xAuthUser);

    StoreFileResponse storeFile(MultipartFile file, String xAuthUser);

    List<String> getListFileNamesByHash(String hash);
}

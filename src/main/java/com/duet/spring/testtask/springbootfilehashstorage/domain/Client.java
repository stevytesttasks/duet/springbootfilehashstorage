package com.duet.spring.testtask.springbootfilehashstorage.domain;

import lombok.*;

import javax.persistence.*;

@Table
@Entity
@Data
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    long id;
    String xAuthUser;
}
